/*
Author:  Ryan Hartman
Course:  COP3530-Data Structures
Semester:  Fall 2011
Project:  Assignment #1
Due Date:  9-19-2011 @5:00PM EST
File Name: "My_String.h"

Description:
This file is the header file for the user-defined class called "My_String".  It contains the class declaration
for "My_String".  The declaration is contained in #ifndef and #endif statements to prevent the class from being
defined multiple times.  The purpose of this class is to replace the C++ string library.
*/

//Preprocessor directives
#include <iostream>

using namespace std;

//If My_string_h has not been yet defined, define it to contain all of the code below, before the #endif statement.
#ifndef My_String_h
#define My_String_h

//Declaration of My_String class
class My_String
{
//Behavior of the class
public:
	//Default constructor prototype.  No formal parameters.
	My_String();
	//Copy constructor prototype.  Takes constant reference to My_String object.
	My_String(const My_String &);
	//Explicit value prototype.  Takes character array.
	My_String(char *);
	//Destructor prototype.  No formal parameters.
	~My_String();
	//IsEmpty() member function prototype.  No formal parameters.
	bool IsEmpty();
	//Length() member function prototype.  No formal parameters.
	int Length();
	//Insert() member function prototype.  Takes character array and integer
	void Insert(char *, int);
	//Remove() member function prototype.  Takes character array.
	void Remove(char *);
	//Print() member function prototype.  No formal parameters.
	void Print();
	//IsEqual() member function prototype.  Takes constant reference to My_String object.
	bool IsEqual(const My_String &);
	//SetEqual() member function prototype.  Takes constant reference to My_String object.
	void SetEqual(const My_String &);
	//Find() member function prototype.  Takes constant reference to My_String object and integer.
	int Find(const My_String &, int);
	//FindLast() member function prototype.  Takes character array.
	int FindLast(char *);
	//AddStrings() member function prototype.  Takes constant reference to My_String object.
	void AddStrings(const My_String &);
//State of the class
private:
	int size;
	char *d_array;
};
#endif