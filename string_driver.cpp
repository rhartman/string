/*
Author:  Ryan Hartman
Course:  COP3530-Data Structures
Semester:  Fall 2011
Project:  Assignment #1
Due Date:  9-19-2011 @5:00PM EST
File Name: "string_driver.cpp"

Description:
This program demonstrates the capabilities of the user-defined class called "My_String".  It will demonstrate
how the default, copy and explicit value constructors work.  Also, it will demonstrate how the member functions
for the class operate.  Information on the class can be found in the respective source files called "My_String.h" and
"My_String.cpp".
*/

//Preprocessor directives
#include <iostream>
#include "My_String.h"

using namespace std;

//The cout statements seem to adequately describe what is going on in this driver.
int main()
{
	My_String A;
	cout<<"Testing IsEmpty(). Is A Empty?: "<<A.IsEmpty()<<endl;
	A.Insert("Hello", 0);
	cout<<"Tested default constructor, Print and Insert functions.  d_array for A = ";
	A.Print();
	A.Insert("123", 2);
	cout<<"Inserted '123' at index 2.  d_array for A = ";
	A.Print();
	A.Remove("123");
	cout<<"Removed '123' using Remove function.  d_array for A = ";
	A.Print();
	My_String B = A;
	cout<<"Tested copy constructor by creating B and assigning it A."<<endl;
	cout<<"d_array for A = ";
	A.Print();
	cout<<"d_array for B = ";
	B.Print();
	cout<<"Testing IsEqual. Does A = B? The answer is: "<<A.IsEqual(B)<<endl;
	B.Insert("12345", 0);
	cout<<"d_array for B = ";
	B.Print();
	B.SetEqual(A);
	cout<<"Testing SetEqual to copy A to B.  d_array for B = ";
	B.Print();
	A.Insert("test", 3);
	B.Remove("Hello");
	B.Insert("test", 0);
	cout<<"d_array for A = ";
	A.Print();
	cout<<"Testing find by searching for query 'test' in A."<<endl;
	cout<<"Test was found at index "<<A.Find(B, 0)<<endl;
	B.Insert("st", 4);
	cout<<"d_array in B = ";
	B.Print();
	cout<<"Testing FindLast on B with query 'st'.  Found 'st' at index "<<B.FindLast("st")<<endl;
	My_String C(" Goodbye");
	cout<<"Tested explicit-value constructor.  d_array for C = ";
	C.Print();
	B.Remove("testst");
	B.Insert("Hello", 0);
	B.AddStrings(C);
	cout<<"Tested AddStrings on B and C.  d_array for B = ";
	B.Print();
	cout<<"Testing Length(). d_array in B is "<<B.Length()<<" characters long."<<endl;
	cout<<"The destructor will be called 3 times:"<<endl;

	return 0;
}