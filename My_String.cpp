/*
Author:  Ryan Hartman
Course:  COP3530-Data Structures
Semester:  Fall 2011
Project:  Assignment #1
Due Date:  9-19-2011 @5:00PM EST
File Name: "My_String.cpp"

Description:
This file is the implementation file for the My_String class.  It contains the definition of all of the member functions
declared in the "My_String.h" file.  The purpose of this class is to act as a replacement to the C++ string library.
*/

//Preprocessor directive
#include <iostream>
#include "My_String.h"

using namespace std;

//Description:  Default constructor.
//Preconditions:  State of My_String object has not been initialized.
//Postconditions:  State of My_String object has been initialized.
My_String::My_String()
{
	size = 0;
	d_array = 0;

	return;
}

//Description:  Copy constructor.
//Preconditions:  A My_String object does or does not contain data.
//Postconditions:  A My_String object contains the same data as another My_String object.
My_String::My_String(const My_String &Orig)
{
	//Size of current object is set to that of Orig
	size = Orig.size;
	//d_array has 'size' units of memory allocated
	d_array = new char[size];

	//Contents of Orig.d_array are put into d_array of the current object
	for(int i = 0; i < size; i++)
	{
		d_array[i] = Orig.d_array[i];
	}

	return;
}

//Description:  Explicit-value construcor.
//Preconditions:  A My_String object does not exist.
//Postconditions:  A My_String object exists with data specified by the user.
My_String::My_String(char *Orig)
{
	int length;
	//Length of Orig is determined and stored in 'length'
	for(length = 0; Orig[length] != '\0'; length++);
	//d_array of current object has 'length' units of memory allocated
	d_array = new char[length];
	//size of current object is set to 'length'
	size = length;
	//Contents of Orig are copied into d_array
	for(int i = 0; i < size; i++)
	{
		d_array[i] = Orig[i];
	}

	return;
}

//Description:  Destructor
//Preconditions:  Dynamic memory exists for a My_String object.
//Postconditions:  Dynamic memory does not exist for a My_String object.
My_String::~My_String()
{
	cout<<"The destructor has been called."<<endl;
	size = 0;
	delete [] d_array;
	d_array = 0;
}

//Description:  IsEmpty().  Determines if a My_String object is empty.
//Preconditions:  User does not know if a My_String object contains anything.
//Postconditions:  User knows if My_String object contains data.
bool My_String::IsEmpty()
{
	if(size == 0)
	{
		return true;
	}
	return false;
}

//Description:   Length().  Tells the user how long a My_String object is.
//Preconditions:  User does not know length of My_String object.
//Postconditions:  User knows the length of My_String object.
int My_String::Length()
{
	return size;
}

//Description:  Insert().  Inserts a substring into a My_String at position 'i'.
//Preconditions:  My_String object contains data.
//Postconditions:  My_String object contains previous data plus user-defined substring.
void My_String::Insert(char *Orig, int i)
{
	int length;
	//Determine length of Orig
	for(length = 0; Orig[length] != '\0'; length++);
	//Determine size of new d_array
	int tempsize = length + size;
	//Allocate dynamic memory for new string 'temp'
	char *temp = new char[tempsize];
	//Fill temp with d_array's data.
	for(int j = 0; j < size; j++)
	{
		temp[j] = d_array[j];
	}

	//The following is executed if i is within the boundaries of d_array
	if(i < size)
	{
		//All characters after i in temp are shifted to make room for contents of Orig
		for(int j = i; j < size; j++)
		{
			temp[j + length] = d_array[j];
		}
		//Orig is inserted into temp at position i
		for(int j = 0; j < length; j++)
		{
			temp[i + j] = Orig[j];
		}
	}
	//The following is executed if 'i' is greater than or equal to size
	if(i >= size)
	{
		//Orig is inserted at the end of pre-existing string
		for(int j = 0; j < length; j++)
		{
			temp[j + size] = Orig[j];
		}
	}
	//De-allocate d_array's dynamic memory.
	delete [] d_array;
	//Pointer d_array now contains the memory address of temp.
	d_array = temp;
	//Size of current object is now tempsize.
	size = tempsize;
}

//Description:  Remove().  Removes a substring from My_String object if found.
//Preconditions:  A My_String object contains data.
//Postconditions:  A My_string object no longer contains a specific substring.
void My_String::Remove(char *query)
{
	int length;
	//Determine length of query
	for(length = 0; query[length] != '\0'; length++);
	//If the query is blank, output a message to the user and exit.
	if(length == 0)
	{
		cout<<"Your search query is blank."<<endl;
		return;
	}
	for(int i = 0; i < size; i++)
	{
		//If d_array at i is equal to the first letter in the query
		if(d_array[i] == query[0])
		{
			int k = 1;
			//Increment k for each subsequent letter that matches in the query, otherwise exit this for loop
			for(int j = i + 1; j < size; j++)
			{
				if(d_array[j] == query[k])
				{
					k++;
				}
				else
				{
					break;
				}
			}
			//If k == length, then the main for loop has iterated to a point where the query is found and the following code is executed
			if(k == length)
			{
				//Allocate new dynamic memory for an array that holds size - length of query
				char *temp = new char[size - length];
				//Shift characters after query down in the array by length of query
				for(int x = i; x < size; x++)
				{
					d_array[x] = d_array[x + length];
				}
				//Fill temp with the new array.
				for(int x = 0; x < size - length; x++)
				{
					temp[x] = d_array[x];
				}
				//De-allocate the dynamic memory for d_array
				delete [] d_array;
				//Pointer d_array now contains memory address of first element in temp.
				d_array = temp;
				//Decrement size by length of query
				size -= length;
				return;
			}
		}
	}
	//If the main for loop iterates completely without encountering a return statement, the query was not found.
	cout<<"The substring was not found in this object."<<endl;
	return;
}

//Description:  Print().  Displays the contents of a My_String object's dynamic array
//Preconditions:  My_String object contains data.
//Postconditions:  My_string object's data is displayed on the screen.
void My_String::Print()
{
	for(int i = 0; i < size; i++)
	{
		cout<<d_array[i];
	}
	cout<<endl;
}

//Description:  IsEqual().  Determines if two My_String objects contain the same data.
//Preconditions:  Two My_String objects exist.
//Postconditions:  The user knows whether or not they contain identical data.
bool My_String::IsEqual(const My_String &query)
{
	if(size == query.size)
	{
		for(int i = 0; i < size; i++)
		{
			if(d_array[i] != query.d_array[i])
			{
				return false;
			}
		}
		return true;
	}
	return false;
}

//Description:  SetEqual().  Sets the current object equal to the My_string object in the actual argument.
//Preconditions:  Current object does or does not contain data.
//Postconditions:  Current object contains data from object in function arguments.
void My_String::SetEqual(const My_String &query)
{
	size = query.size;
	for(int i = 0; i < size; i++)
	{
		d_array[i] = query.d_array[i];
	}
	return;
}

//Description:  Find().  This function will search for a substring at a position specified by the user
//Preconditions:  A My_String object contains data
//Postconditions:  the index is returned if the query is found, otherwise -1 is returned.
int My_String::Find(const My_String &query, int pos)
{
	//If the position is out of bounds, the user is alerted and -1 is returned.
	if(pos > size)
	{
		cout<<"Your search position is outside the string's boundaries."<<endl;
		return -1;
	}
	//For loop begins iterating at user-defined position.
	for(int i = pos; i < size; i++)
	{
		//If the letter at 'i' matches the first in the search query
		if(d_array[i] == query.d_array[0])
		{
			//if the search query is only one character long, return the index
			if(query.size == 1)
			{
				return i;
			}
			//k is equal to index 1, which is the second letter of the query
			int k = 1;
			//j is i + 1 to iterate through a section of the current object's d_array.
			for(int j = i + 1; j < size; j++)
			{
				//If next letter matches, increment k and check again
				if(d_array[j] == query.d_array[k])
				{
					k++;
				}
				//If there is no match then the query was not yet found, and this for loop ends.
				else
				{
					break;
				}
			}
			//Once the main for loop iterates to the point where the query is found, the index of the first letter is returned.
			if(k == query.size)
			{
				return i;
			}
		}
	}
	//If the main for loop iterates to completion without encountering a return statement, return -1.  The query was not found.
	return -1;
}

//Description:  FindLast().  This function will search for a substring starting at the end of the current d_array
//Preconditions:  A My_String object contains data
//Postconditions:  the index is returned if the query is found, otherwise -1 is returned.
int My_String::FindLast(char *query)
{
	int length;
	//Determine length of query
	for(length = 0; query[length] != '\0'; length++);
	//Loop iterates backwards through the d_array of the current object
	for(int i = size; i >= 0; i--)
	{
		//If the current letter in d_array matches the first letter in the query, execute the following
		if(d_array[i] == query[0])
		{
			//If the length of the query is 1 return 'i', the current index
			if(length == 1)
			{
				return i;
			}
			//match is set t 1, which is the index of the second letter in the query
			int match = 1;
			//k is always 1 ahead of i, so that the subsequent letters in query can be compared to those of d_array
			for(int k = i + 1; k < size; k++)
			{
				//increment match if the letters are equal at the below positions
				if(d_array[k] == query[match])
				{
					match++;
				}
				//Otherwise, break out of this for loop and let the main loop iterate until a return statement is encountered
				else
				{
					break;
				}
				//If match is equal to length, return 'i', the current index in the main for loop
				if(match == length)
				{
					return i;
				}
			}
		}
	}
	//This statement is encountered when the main for loop iterates completely without encountering any other return statements
	return -1;
}

//Description:  AddStrings().  This function adds query to the back of the current object's d_array
//Preconditions:  Two My_String objects contain data
//Postconditions:  The current object contains its data + the data of query
void My_String::AddStrings(const My_String &query)
{
	//If query is empty, exit
	if(query.size == 0)
	{
		return;
	}
	//Allocate enough memory to hold both d_array and the query's data
	char *temp = new char[size + query.size];
	//Copy d_array into temp.
	for(int i = 0; i < size; i++)
	{
		temp[i] = d_array[i];
	}
	//Copy query.d_array into temp after the current object's d_array.
	for(int i = 0; i < (query.size); i++)
	{
		temp[i + size] = query.d_array[i];
	}
	//De-allocate dynamic memory for the current object's d_array
	delete [] d_array;
	//Change current object's size
	size = size + query.size;
	//Pointer d_array now points to temp's 1st memory address.
	d_array = temp;
	//Show's over.  Go home.
	return;
}